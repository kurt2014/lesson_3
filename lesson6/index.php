<?php
session_start();
/* Задача 1 на '.', символы:
1) Дана строка 'ahb acb aeb aeeb adcb axeb'. Напишите регулярку, которая найдет строки ahb, acb, aeb по шаблону: буква 'a', любой символ, буква 'b'.
2) Дана строка 'aba aca aea abba adca abea'. Напишите регулярку, которая найдет строки abba adca abea по шаблону: буква 'a', 2 любых символа, буква 'a'.*/

// 1) 
echo preg_replace('/a.b/', '!', 'ahb acb aeb aeeb adcb axeb');
br_hr(1);

// 2)
echo preg_replace('/a..a/', '!', 'aba aca aea abba adca abea');
br_hr(2);

/* Задача 2 на '+', '*', '?', ():
1) Дана строка 'aa aba abba abbba abca abea'. Напишите регулярку, которая найдет строки aba, abba, abbba по шаблону: буква 'a', буква 'b' любое количество раз, буква 'a'. 
2) Дана строка 'aa aba abba abbba abca abea'. Напишите регулярку, которая найдет строки aa, aba, abba, abbba по шаблону: буква 'a', буква 'b' любое количество раз (в том числе ниодного раза), буква 'a'.*/

// 1)
echo preg_replace('/ab+a/', '!', 'aa aba abba abbba abca abea');
br_hr(1);

// 2)
echo preg_replace('/ab*?a/', '!', 'aa aba abba abbba abca abea');
br_hr(2);
/* Задача 3 на экранировку:
1) Дана строка 'a.a aba aea'. Напишите регулярку, которая найдет строку a.a, не захватив остальные. 
2) Дана строка '23 2+3 2++3 2+++3 345 567'. Напишите регулярку, которая найдет строки 2+3, 2++3, 2+++3, не захватив остальные (+ может быть любое количество).*/

// 1)
echo preg_replace('/a\.a/', '!', 'a.a aba aea');
br_hr(1);

// 2)
echo preg_replace('/2\+{1,5}3/', '!', '23 2+3 2++3 2+++3 345 567');
br_hr(2);

/* Задача 4 на жадность:
Дана строка 'aba accca azzza wwwwa'. Напишите регулярку, которая найдет все строки по краям которых стоят буквы 'a', и заменит каждую из них на '!'. Между буквами a может быть любой символ (кроме a).*/

echo preg_replace('/a.+?a/', '!', 'aba accca azzza wwwwa');
br_hr(2);

/* Задача 5 на {}:
1) Дана строка 'aa aba abba abbba abbbba abbbbba'. Напишите регулярку, которая найдет строки abba, abbba, abbbba и только их. 
2) Дана строка 'aa aba abba abbba abbbba abbbbba'. Напишите регулярку, 	которая найдет строки вида aba, в которых 'b' встречается менее 3-х раз (включительно).*/

//1)
echo preg_replace('/ab{2,4}a/', '!', 'aa aba abba abbba abbbba abbbbba');
br_hr(1);

// 2)
echo preg_replace('/ab{1,3}a/', '!', 'aa aba abba abbba abbbba abbbbba');
br_hr(2);

/* Задача 6 на \s, \S, \w, \W, \d, \D:
1) Дана строка 'a1a a2a a3a a4a a5a aba aca'. Напишите регулярку, которая найдет строки, в которых по краям стоят буквы 'a', а между ними одна цифра. 
2) Дана строка 'ave a#a a2a a$a a4a a5a a-a aca'. Напишите регулярку, которая заменит все пробелы на '!'. 	*/

// 1)
echo preg_replace('/a\da/', '!', 'a1a a2a a3a a4a a5a aba aca');
br_hr(1);

// 2)
echo preg_replace('/\s/', '!', 'ave a#a a2a a$a a4a a5a a-a aca');
br_hr(2);


/* Задача 7 на [], '^' - не, [a-zA-Z], кириллицу:
1) Дана строка 'aba aea aca aza axa'. Напишите регулярку, которая найдет строки aba, aea, axa, не затронув остальных.
2) Напишите регулярку, которая найдет строки следующего вида: по краям стоят буквы 'a', а между ними - цифра от 3-х до 7-ми. */

// 1)
echo preg_replace('/a[bex]a/', '!', 'aba aea aca aza axa');
br_hr(1);

// 2)
echo preg_replace('/a[3-7]a/', '!', 'a1a a2a a3a a4a a5a a6a a7a a8a a9a a10a');
br_hr(2);

/*Задача 8 на [a-zA-Z] и квантификаторы:
1) Дана строка 'aAXa aeffa aGha aza ax23a a3sSa'. Напишите регулярку, которая найдет строки следующего вида: по краям стоят буквы 'a', а между ними - маленькие латинские буквы, не затронув остальных.	
2) Дана строка 'ааа ббб ёёё ззз ййй ААА БББ ЁЁЁ ЗЗЗ ЙЙЙ'. Напишите регулярку, которая найдет все слова по шаблону: любая кириллическая буква любое количество раз.*/

// 1)
echo preg_replace('/a[a-z]a/', '!', 'aAXa aeffa aGha aza ax23a a3sSa');
br_hr(1);

// 2)
echo preg_replace('/[аб-яА-яЁё]+/', '!', 'ааа ббб ёёё ззз ййй ААА БББ ЁЁЁ ЗЗЗ ЙЙЙ');
br_hr(2);

/* Задача 9 на '^', '$':
Дана строка 'aaa aaa aaa'. Напишите регулярку, которая заменит первое 'aaa' на '!'.*/

echo preg_replace('/^a{3}/', '!', 'aaa aaa aaa');
br_hr(2);

/* Задача 10 на '|':
Дана строка 'aeeea aeea aea axa axxa axxxa'. Напишите регулярку, которая найдет строки следующего вида: по краям стоят буквы 'a', а между ними - или буква 'e' любое количество раз или по краям стоят буквы 'a', а между ними - буква 'x' любое количество раз.*/

echo preg_replace('/a[e|x]+a/', '!', 'aeeea aeea aea axa axxa axxxa');
br_hr(2);

/* Задача 11 на экранировку посложнее:
Дана строка 'bbb /aaa\ bbb /ccc\'. Напишите регулярку, которая найдет содержимое всех конструкций /...\ и заменит их на '!'.*/

echo preg_replace('/\/[a-zA-z\d\S]+\\\/', '!', 'bbb /aaa\ bbb /ccc\\');
br_hr(2);

/* Задача 12 на preg_match[_all]:
1)С помощью preg_match определите, что переданная строка является емэйлом. Примеры емэйлов для тестирования mymail@gmail.com, my.mail@gmail.com, my-mail@gmail.com, my_mail@gmail.com, mail@mail.com, mail@mail.by, mail@yandex.ru.
2) Дана строка с текстом, в котором могут быть емейлы. С помощью preg_match_all найдите все емэйлы.
3) С помощью preg_match определите, что переданная строка является доменом. Протокол может быть как http, так и https. Домен может быть со слешем в конце: http://site.ru, http://site.ru/, https://site.ru, https://site.ru/ */

// 1)
echo preg_match('/[a-zA-Z-.]+@[a-z]+\.[a-z]{2,3}/', 'my_mail@gmail.com');
br_hr(1);

// 2)
$email = 'mymail@gmail.com, my.mail@gmail.com, my-mail@gmail.com, my_mail@gmail.com, mail@mail.com, mail@mail.by, mail@yandex.ru';
echo preg_match_all('/[a-zA-Z-.]+@[a-z]+\.[a-z]{2,3}/', $email, $quantity);
br_hr(1);
print_r($quantity);
br_hr(1);

// 3)
echo preg_match('/[htp|s]+:\/\/[a-z]+\.[a-z]+\/?/', 'https://site.ru/');
br_hr(2);

/* Задача 13 на preg_replace:
С помощью только preg_replace замените в строке домены вида http://site.ru, http://site.com на <a href="http://site.ru">site.ru</a>.*/
// Честно говоря не очень понял задание поэтому 1-й вариант:
echo preg_replace('/^[htp|s]+:\/\/[a-z0-9]+\.[a-z]+\/?$/', '<a href="http://site.ru">site.ru</a>', 'http://site.ru');
br_hr(1);
echo preg_replace('/^[htp|s]+:\/\/[a-z0-9]+\.[a-z]+\/?$/', '<a href="http://site.ru">site.ru</a>', 'http://site.com');
br_hr(1);

// И 2-й:
$string = 'http://site.ru, http://site.com';
echo preg_replace('/https?:\/\/[a-z0-9]+\.[a-z]{2,3}/', "<a href='http://site.ru'>site.ru</a>", $string);
br_hr(2);

/* Задача 14 на cookie:
1) По заходу на страницу запишите в куку с именем test текст '123'. Затем обновите страницу и выведите содержимое этой куки на экран. 	
2) Удалите куку с именем test. 	
3) Сделайте счетчик посещения сайта посетителем. Каждый раз, заходя на сайт, он должен видеть надпись: 'Вы посетили наш сайт % раз!'. 	
4) Спросите дату рождения пользователя. При следующем заходе на сайт напишите сколько дней осталось до его дня рождения. 	Если сегодня день рождения пользователя - поздравьте его. */

echo "<a href=\"cookie_session/cookie1.php\">Задача 14.1,2</a>";
br_hr(1);
echo "<a href=\"cookie_session/cookie3.php\">Задача 14.3</a>";
br_hr(1);
echo "<a href=\"cookie_session/cookie4.php\">Задача 14.4</a>";
br_hr(2);

/* Задача 15 на sessions:
1) По заходу на страницу запишите в сессию текст 'test'. Затем обновите страницу и выведите содержимое сессии на экран. 	
2) Пусть у вас есть две страницы сайта. Запишите на первой странице что-нибудь в сессию, а затем выведите это при заходе на другую страницу. 	
3) Сделайте счетчик обновления страницы пользователем. 		Данные храните в сессии. Скрипт должен выводить на экран количество обновлений. При первом заходе на страницу он должен вывести сообщение о том, что вы еще не обновляли страницу. 
4) Спросите у пользователя email с помощью формы. Затем сделайте так, чтобы в другой форме (поля: имя, фамилия, пароль, email) при ее открытии поле email было автоматически заполнено. */

echo "<a href=\"cookie_session/session1.php\">Задача 15.1</a>";
br_hr(1);
echo "<a href=\"cookie_session/session2.php\">Задача 15.2</a>";
br_hr(1);
echo "<a href=\"cookie_session/session3.php\">Задача 15.3</a>";
br_hr(1);
echo "<a href=\"cookie_session/session4.php\">Задача 15.4</a>";
br_hr(2);


// выведет <br> в других случаях <hr>
function br_hr($a)
{
    if ($a === 1) {
        echo "<br>";
    } else {
        echo "<hr>";
    }
}