<?php
session_start();
if (empty($_SESSION['count'])) {
    $_SESSION['count'] = 1;
    echo "Вы еще не обновляли страницу.";
} else {
    $_SESSION['count'] += 1;
    echo "Вы обновляли страницу " . $_SESSION['count'] . " раз";
}
echo "<br>";
echo "<a href=\"../index.php\">Главная</a>";
