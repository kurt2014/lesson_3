<?php

/* Задача 1:
1) Пусть в корне вашего сайта лежит файл test.txt. 	
2) Считайте данные из этого файла и выведите их на экран.*/

$file = "test.txt";

if (file_exists($file)) {
    $content = file_get_contents($file);
    echo $content;
} else {
    echo "ERROR: File does not exist.";
}
br_hr(2);

/* Задача 2:
1) Пусть в корне вашего сайта лежит файл test.txt.
2) Запишите в него текст '12345'.*/

if (file_exists($file)) {
    $handle = fopen($file, "w");
    fwrite($handle, '12345');
    $content = fread($handle, filesize($file));
    fclose($handle);
} else {
    echo "ERROR: File does not exist.";
}
br_hr(2);

/* Задача 3:
1) Создайте файл test.txt и запишите в него текст '12345'.
2) Пусть изначально файла с таким именем не существует.*/

if (file_exists($file)) {
    if (unlink($file)) {
        echo "File removed successfully.";
    } else {
        echo "ERROR: File cannot be removed.";
    }
} else {
    echo "ERROR: File does not exist.";
}

// 1) Создайте файл test.txt и запишите в него текст '12345'

if (!file_exists($file)) {
    $handle = fopen("test.txt", "w");
    fwrite($handle, '12345');
    $content = fread($handle, filesize("test.txt"));
    fclose($handle);
} else {
    echo "ERROR: File does not exist.";
}
br_hr(2);

/* Задача 4:
1) Пусть в корне вашего сайта лежит файл test.txt, в котором записан текст '12345'.
2) Откройте этот файл, запишите в конец текста восклицательный знак и сохраните новый текст обратно в файл. */

if (file_exists($file)) {
    $handle = fopen($file, "a");
    fwrite($handle, '!');
    fclose($handle);
} else {
    echo "ERROR: File does not exist.";
}
br_hr(2);

/* Задача 5:
1) Пусть в корне вашего сайта лежит файл count.txt.
2) Изначально в нем записано число 0.
3) Сделайте так, чтобы при обновлении страницы наш скрипт каждый раз увеличивал это число на 1. 
4) То есть у нас получится счетчик обновления страницы в виде файла. */
if (file_exists("count.txt")) {
    $count = file_get_contents('count.txt');
    file_put_contents('count.txt', $count + 1);
    echo file_get_contents('count.txt');
} else {
    echo "Файл не существует";
}
br_hr(2);

/* Задача 6:
1) Пусть в корне вашего сайта лежат файлы 	1.txt, 2.txt и 3.txt.
2) Вручную сделайте массив с именами этих файлов.
3) Переберите его циклом, прочитайте содержимое каждого из файлов, слейте его в одну строку и запишите в новый файл new.txt.
4) Изначально этого файла не должно быть. */

if (!file_exists("1.txt") && (!file_exists("2.txt")) && (!file_exists("3.txt"))) {
    $txt1 = fopen("1.txt", "w");
    fclose($txt1);
    $txt2 = fopen("2.txt", "w");
    fclose($txt2);
    $txt3 = fopen("3.txt", "w");
    fclose($txt3);
    $array = ['1.txt', '2.txt', '3.txt'];
    $fail = '1';

    foreach ($array as $element) {
        $fail .= file_get_contents($element);
    }

    file_put_contents('new.txt', $fail);
    echo file_get_contents('new.txt');
} else {
    echo "Операция уже выполнена";
}
br_hr(2);

/* Задача 7:
1) Пусть в корне вашего сайта лежит файл old.txt. 
2) Переименуйте его на new.txt. */

$old = "old.txt";
// Есле файла нет создаем
if (!file_exists($old)) {
    $old = fopen("old.txt", "w");
    fclose($old);
} // В противном случае переименовываем
else {
    rename('old.txt', 'new.txt');
}
br_hr(2);

/* Задача 8:
1) Пусть в корне вашего сайта лежит файл test.txt.
2) Пусть также в корне вашего сайта лежит папка dir.
3) Переместите файл в эту папку.*/
$test = "test.txt";

if (!is_dir('dir')) {
    mkdir('dir');
} else {
    rename($test, "dir/test.txt");
}
br_hr(2);

/* Задача 9:
1) Пусть в корне вашего сайта лежит файл test.txt.
2) Скопируйте его в файл copy.txt.*/
if (file_exists("test.txt")) {
    copy('test.txt', 'copy.txt');
} else {
    $test = fopen("test.txt", "w");
    fclose($test);
    copy('test.txt', 'copy.txt');
}

br_hr(2);

/* Задача 10:
1) Пусть в корне вашего сайта лежит файл test.txt.
2) Удалите его. */
if (file_exists("test.txt")) {
    unlink("test.txt");
} else {
    echo "Файл уже удален";
}
br_hr(2);

/* Задача 11:
Проверьте, лежит ли в корне вашего сайта файл test.txt. */

if (file_exists('test.txt')) {
    echo "Файл существует";
} else {
    echo "Файл не существует";
}
br_hr(2);

/* Задача 12:
1) Пусть в корне вашего сайта лежит файл test.txt. 
2) Узнайте его размер, выведите на экран.
*/
if (file_exists('test.txt')) { // Есле файл существует
    echo "Размер файла " . filesize('test.txt'); // Узнаем размер
} else {
    echo 'Файл не существует';
}
br_hr(2);

/* Задача 13:
1) Положите в корень вашего сайта какую-нибудь картинку размером более мегабайта.
2) Узнайте размер этого файла и переведите его в мегабайты. */

if (file_exists('diana.jpg')) {
    echo round(filesize('diana.jpg') / (1024 * 1024), 2) . " мегабайтa";
} else {
    echo 'Файл не существует';
}
br_hr(2);

/* Задача 14:
1) Дан файл test.txt.
2) Прочитайте его текст, получите массив его строк. */
if (file_exists("tets.txt")) {
    $test = file_get_contents('test.txt');
    $array = explode(",", $test);
    print_r($array);
} else {
    echo "Операция совершена";
}
br_hr(2);

/* Задача 15:
1) Дан файл test.txt. 
2) В нем на каждой строке написано какое-то число. 
3) С помощью функции file найдите сумму этих чисел и выведете ее на экран. */
if (file_exists("test.txt")) {
    $arr = fopen("test.txt", "a+t");
    fwrite($arr, "14\n88\n14\n88\n");
    fclose($arr);

    $array = file('test.txt');
    $sum = array_sum($array);
    echo $sum;
} else {
    echo "Файл не существует";
}
br_hr(2);


/* Задача 16:
Создайте в корне вашего сайта папку с названием dir. */

if (is_dir('dir')) {
    echo "Папка уже существует";
} else {
    mkdir('dir');
}
br_hr(2);
/* Задача 17:
1) Создайте в корне вашего сайта папку с названием test.
2) Затем создайте в этой папке 3 файла: 1.txt, 2.txt, 3.txt.*/

if (is_dir('test')) {
    echo "Папка уже существует";
} else {
    mkdir('test');
    $array = ['1.txt', '2.txt', '3.txt'];
    foreach ($array as $element) {
        file_put_contents($element, '');
        rename($element, 'test/' . $element);
    }
}
br_hr(2);

/*Задача 18:
Удалите папку с названием dir. */
if(is_dir('dir')) {
    if(file_exists('dir/')) {
        foreach (glob('dir/*') as $file) {
            unlink($file);
        }
    }
    rmdir('dir');
}
else {
    echo "Папка удалена";
}
br_hr(2);

/* Задача 19:
1) Пусть в корне вашего сайта лежит папка old.
2) Переименуйте ее на new. */
if (is_dir('old')) {
    rename('old', 'new');
} elseif (is_dir('new')) {
    echo "Папка уже переименована";
} else {
    echo "Ошибка: Задача 19";
}
br_hr(2);
/* Задача 20:
1) Пусть в корне вашего сайта лежит папка dir, а в ней какие-то текстовые файлы.
2) Переберите эти файлы циклом и выведите их тексты в браузер. */
if (is_dir('dir')) {
    $files = array_diff(scandir('dir'), ['..', '.']);
    foreach ($files as $elem) {
        $fail = file_get_contents('dir/' . $elem);
        echo $fail . '<br>';
    }
} else {
    echo "Такой папки нет";
}
br_hr(2);

/* Задача 21:
1) Пусть дан путь к файлу.
2) Проверьте, файл это или папка */
if (is_file('img/diana.jpg')) {
    echo 'Это файл';
} elseif (is_dir('img/diana.jpg')) {
    echo 'Это папка';
} else {
    echo "Неверные данные";
}
br_hr(2);
/* Задача 22:
1) Сделайте форму для загрузки 1 картинки.
2) Напишите PHP код для валидации и загрузки картинки в отдельную папку в проекте. */

/* Advanced

Задача 1:
1) Создайте файл new.txt с пустым текстом.
2) Пусть изначально файла с таким именем не существует.*/
if (!file_exists("new.txt")) {
    file_put_contents('new.txt', '');
} else {
    echo "Файл уже существует";
}
br_hr(2);
/* Задача 2:
1) Дан массив с именами файлов ['1.txt', '2.txt', '3.txt'].
2) Переберите его циклом и создайте файлы 	с такими именами и пустым текстом.	*/
if (!file_exists("1.txt") && (!file_exists("2.txt")) && (!file_exists("3.txt"))) {
    $array = ['1.txt', '2.txt', '3.txt'];
    foreach ($array as $element) {
        file_put_contents($element, '');
    }
} else {
    echo "Файлы уже созданы";
}
br_hr(2);

/* Задача 4:
1) Пусть в корне вашего сайта лежат файлы 1.txt, 2.txt и 3.txt. 
2) Вручную сделайте массив с именами этих файлов.
3) Переберите его циклом и в текст каждого файла в конец запишите восклицательный знак.*/

$array = ['1.txt', '2.txt', '3.txt'];
$variable = "";
foreach ($array as $element) {
    $variable = file_get_contents($element);
    file_put_contents($element, $variable . "!");
    echo file_get_contents($element) . "<br>";
}

// выведет <br> в других случаях <hr>
function br_hr($a)
{
    if ($a === 1) {
        echo "<br>";
    } else {
        echo "<hr>";
    }
}
