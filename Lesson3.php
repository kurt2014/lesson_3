<?
/*Задача 8:
a) есть переменная $language. В ней может быть 2 варианта значений: 'en_GB' и ‘ru_RU‘';
b) нужно написать код, который в случае, если $language имеет значение 'en_GB' -  в переменную $months запишет массив из 12 месяцев года на английском языке, если language === ‘ru_RU’ – то на русском;
c) нужно выполнить задание с помощью 3 способов: через 2 if, через switch-case, через многомерный массив и без if’ов и switch-case. */
$language = "ru_RU";
$en_GB = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
$ru_RU = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

if ($language === 'en_GB') {
    $mounts = $en_GB;
}
if ($language === 'ru_RU') {
    $mounts = $ru_RU;
}
print_r($mounts);
echo "<br>";

switch ($language) {
    case 'ru_RU':
        print_r($mounts);
        echo "<br>";
        break;
    case 'en_GB':
        print_r($mounts);
        echo "<br>";
        break;
}
echo "<hr>";
/* Чесно питался через массив как то это сделать но никак */
$arr = array("en_GB" => ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    "ru_RU" => ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]
);
foreach ($arr as $leng => $month) {
    foreach ($month as $key => $value) {
        echo " " . $value;
    }
}
echo "<br>";

print_r($language = $language === 'en_GB' ? $ru_RU : $en_GB);
echo "<br>";

echo "<hr>";

/*Задача 13 (на циклы):
a) нужно вывести в столбец числа от 1 до 75;
b) нужно использовать 2 разных цикла.
*/
$namber = 1;
/* Просто через список красиво выглядело */
echo "<select>";
while ($namber <= 75) {
    echo "<option>$namber</option>\n";
    $namber++;
}
echo "</select>" . "<br>";

echo "<hr>";

$namber = 1;
while ($namber <= 75) {
    echo $namber . "<br>";
    $namber++;
}

echo "<hr>";

for ($namber = 1; $namber <= 75; $namber++) {
    echo $namber . "<br>";
}

echo "<hr>";

/*Задача 14 (на циклы):
a) есть массив с элементами 7, 5, 3, 1;
b) выведите квадрат каждого числа с новой строки.
*/
$array = [7, 5, 3, 1];
foreach ($array as $key) {
    echo $key * $key . "<br>";
}
echo "<hr>";

/*Задача 15 (на циклы):
a) есть массив $responseStatuses = [
   100 => 'Сообщения успешно приняты к отправке',
   105 => 'Ошибка в формате запроса',
   110 => 'Неверная авторизация',
   115 => 'Недопустимый IP-адрес отправителя',
];
b) с помощью цикла выведите на экран столбец строк формата: ‘Код ошибки  - 100, текст ошибки - Сообщения успешно приняты к отправке’ (всего 4 строки). */
$responseStatuses = [
    100 => "Сообщения успешно приняты к отправке",
    105 => "Ошибка в формате запроса",
    110 => "Неверная авторизация",
    115 => "Недоступный IP-aдрес отправителя",
];
foreach ($responseStatuses as $key => $value) {
    echo $key . " " . $value . "<br>";
}
echo "<hr>";
echo "<hr>";
/*Advanced
Задача 1:
a) есть число 5000;
b) нужно поделить его на 2 столько раз, пока результат делениā не станет меньше 77;
c) выведите в конце число, которое получитса;
d) посчитайте и выведите количество итераций (повторений цикла), которое понадобилось;
e) нужно решить задачу с помощю двух разных циклов.*/

for ($number = 5000, $i = 0; $number > 77; $number = $number / 2, $i++) ;
echo $i . "<br>"; // Итераций
echo floor($number) . "<br>"; // Число

// 2 Цикл
$numeric = 5000;
$i = 0;
while ($numeric > 50) {
    $numeric = $numeric / 2;
    $i++;
}
echo $i . "<br>";
echo floor($numeric) . "<br>";
echo "<hr>";

/* Задача 2:
a) есть такой массив;
b) нужно перебрать его в цикле и записать в переменную $result список всех id (и ids) через
точку с запāтой (;);
c) вывести значение переменной $result на екран.*/
$arr = [
    'members' => [
        [
            'type' => 'user_to',
            'id' => 14,
            'last_view' => 1483023775,
            'username' => 'Petya',
        ],
        [
            'type' => 'user_from',
            'id' => 16,
            'ids' => [1, 5, 6, 10],
            'last_view' => 1483023775,
            'username' => 'Vova',
        ],
        'simple_user',
        [
            'type' => 'user_next',
            'id' => 26,
            'last_view' => 1483023772,
            'username' => 'Andrey',
        ],
        'advanced_user' => 'Sergei',
    ],
];
foreach ($arr as $user) {
    $arry = array_column($user, 'ids');
    print_r($arry);
}

echo "<br>";
foreach ($arr as $user) {
    $arry = array_column($user, 'id');
    print_r($arry);
}
echo "<br>";
/*Смог только так очень интересно как правильно(((*/

echo "<hr>";

/*Задача 3:
a) исполþзуā цикл for создайте массив из чисел от 1 до 77;
b) пример: [1, 2, 3, …, 77]
*/

$for = [];
for ($a = 1; $a <= 77; $a++) {
    $for[] = $a;
}

print_r($for);
echo "<br>";

/*Именно как в примере пытался вывести но не получилось
Но есть столбик*/
function arrayList($list)
{
    foreach ($list as $value) {
        echo $value . ' </br>';
    }
}

echo arrayList($for);
