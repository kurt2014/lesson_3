/*������ 1:
1) �������� sql-������� ��� �������� ������� workers, ��� �� ��������� ����;
2) ����� �������� �������, �������� � ��� ������� last_login, ���� int, ������� �� ����� ���� ������;
3) �������� ��� ���� ������� test ������������� ���� � ������� ��;
4) ������������ ������� last_login � last_login_time;
5) �������� ��������� �������� last_login_time �� 0;
6) �������� ������� last_login_time, ����� ��� ������ ����� ���� ������; 
7) ��������� ������� workers ������� (� ���� ������), ��� �� ���������, ������ �������� �������������� ������.
8) �������� 2 ����������� � ����� (��������� ����) ����� �� ����� �������.*/

CREATE TABLE workers(
 id SERIAL PRIMARY KEY,
 name VARCHAR (50) UNIQUE NOT NULL,
 age integer,
 salary integer NOT NULL
);

-- �������� ������� � ����� INTEGER
ALTER TABLE workers ADD COLUMN last_login INTEGER;

-- ��������� ���� ������� �� �� ������
ALTER TABLE workers ALTER COLUMN last_login SET NOT NULL;

-- �������� �������
ALTER TABLE workers ADD COLUMN test VARCHAR;

-- �������� �������
ALTER TABLE workers DROP COLUMN test;

-- ������������� �������
ALTER TABLE workers RENAME COLUMN last_login TO last_login_time;

-- �������� ��������� ��������
ALTER TABLE workers ALTER COLUMN last_login_time SET DEFAULT 0;

-- �������� ����� ��� �� ����� ���� ������
ALTER TABLE workers ALTER COLUMN last_login_time DROP NOT NULl;

-- ��������� �������� �������
INSERT INTO workers ( name, age, salary)
    VALUES ('����', 23, 400),
               ('����', 25, 500),
               ('����', 23, 500),
               ('����', 30, 1000),
               ('����', 27, 500),
               ('������', 28, 1000);
-- ������� ����������� ������� � ����� �������
COMMENT ON COLUMN workers.age IS '�������';
COMMENT ON COLUMN workers.salary IS '��������';

-- ������ 2: ������� ��������� � id = 3
select * from workers where id = 3;

-- ������ 3: ������� ���������� � ��������� 1000$.
SELECT * from workers where salary = 1000;

-- ������ 4: ������� ���������� � �������� 23 ����.
select * from workers where age = 23;

-- ������ 5: ������� ���������� � ��������� ����� 400$.
select * from workers where salary > 400;

-- ������ 6: ������� ���������� � ��������� ������ ��� ������� 500$.
select * from workers where salary >= 500;

-- ������ 7: ������� ���������� � ��������� �� ������ 500$. 
select * from workers where salary != 500;

-- ������ 8: ������� ���������� � ��������� ������ ��� ������� 900$.
select * from workers where salary <= 900;

-- ������ 9: ������� �������� � ������� ����.
select salary,age from workers where name like '����'; 

--��� ���,(������ �� �� ����� ����� ������� �� ����� ��� ����� ���� �������), ������ (����������� ��������� ������)
select * from workers where name like '����'; 

-- ������ 10: ������� ���������� � �������� �� 25 (�� ������������) �� 28 ��� (������������).
select * from workers where (age > 25) and (age <= 28); 

-- ������ 11: ������� ��������� ����.
select * from workers where name = '����';

-- ������ 12: ������� ���������� ���� � ����.
-- ������ �������� ��� ���������� ����(�������� �����������)
select * from workers where (NAME = '����') or (name = '����');

-- ������ 13: ������� ����, ����� ��������� ����.
select * from workers where name != '����';

-- ������ 14: ������� ���� ���������� � �������� 27 ��� ��� � ��������� 1000$.
select * from workers where (age = 27) or (salary = 1000); 

-- ������ 15: ������� ���� ���������� � �������� �� 23 ��� (������������) �� 27 ��� (�� ������������) ��� � ��������� 1000$.
select * from workers where ((age >= 23) and (age < 27)) or (salary = 1000); --���� ��� ����� (salary = 1000) ������ �� ����� �� ��� ����������� ��������

-- ������ 16: ������� ���� ���������� � �������� �� 23 ��� �� 27 ��� ��� � ��������� �� 400$ �� 1000$. 
select * from workers where ((age >= 23) and (age <= 27)) or (salary >= 400) and (salary <= 1000); 

--��� ���, ������� ������ �� ������ � � ���������� �������� ������� ��� �������
select * from workers where ((age > 23) and (age < 27)) or (salary > 400) and (salary < 1000);

-- ������ 17: ������� ���� ���������� � �������� 27 ��� ��� � ��������� �� ������ 400$.
select * from workers where (age = 27) or (salary != 400); 

/*������ 18 (�� UPDATE, 27 ������ � ����� � ��������� ��� ���������):
1) ��������� ���� �������� � 200$.
2) ��������� � id=4 ��������� ������� 35 ���.
3) �������� ���� � id 2 � 4, �������� � ��� (� ���� last_login_time) ������� Unix-�����;*/ 
update workers set salary = 200 where name = '����';
update workers set age = 35 where id = 4;

-- ������� ��� �� � ����, ���� �����, ������� ������ �� �������, � �� � ��������� ��������� ��� ������ ��������.
/*update workers set last_login_time = timestamp where id = 2 and id = 4;*/

/*������ 19: (�� DELETE, 28 ������ � ����� � ��������� ��� ���������):
������� ����.
������� ���� ����������, � ������� ������� 23 ����.*/
DELETE from workers where name = '����';
delete from workers where age = 23;