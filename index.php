<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title> Lesson5 </title>
</head>
<body>

<!-- Задача 1:
1) Спросите у пользователя имя, фамилию, автобиографию (textarea).
2) Проверьте, чтобы это были строки и в поле `автобиография`не было тегов.
3) Выведите эти данные на экран в формате:
Иванов Иван
Родился …
Вырос...
4) Позаботьтесь о том, чтобы пользователь не мог вводить теги и таким образом сломать сайт. 
5) Сделайте так, чтобы после отправки формы значения ее полей не пропадали. -->

<form action="" method="POST">
    <p>Введите ваше имя</p>
    <!-- Чтобы после отправки формы значения ее полей не пропадали -->
    <p><input name="name" type="text" placeholder="Имя"
              value="<?php if (isset($_REQUEST['name'])) echo $_REQUEST['name']; ?>"></p>
    <p>Введите вашу фамилию</p>
    <p><input name="surname" type="text" placeholder="Фамилия"
              value="<?php if (isset($_REQUEST['surname'])) echo $_REQUEST['surname']; ?>"></p>
    <p>Введите вашу автобиографию</p>
    <p><textarea name="autobiography" wrap="soft"></textarea></p>
    <p><input type="submit"></p>
</form>

<?php
// Убираем теги
echo strip_tags((string)$_POST['name']) . "<br>";
echo strip_tags((string)$_POST['surname']) . "<br>";
echo strip_tags((string)$_POST['autobiography']) . "<br>";
?>
<hr>

<!-- Задача 2:
1) Сделайте 3 поля ввода, в которые вводятся день, месяц и год.
2) Проверьте, что данные в этих полях - числа.
3) Выведите дату в формате '2019-04-04' -->

<form action="" method="POST">
    <p><input name="day" type="number" placeholder="День"></p>
    <p><input name="month" type="number" placeholder="Месяц"></p>
    <p><input name="year" type="number" placeholder="Год"></p>
    <p><input type="submit"></p>
</form>
<?php
if (isset($_POST["day"]) && isset($_POST["month"]) && isset($_POST["year"])) {
    $day = $_POST["day"];
    $month = $_POST["month"];
    $year = $_POST["year"];
    if (checkdate((int)$month, (int)$day, (int)$year)) {
        echo $year . "-" . $month . "-" . $day;
    } else {
        echo "Чет не так";
    }
}
?>
<hr>

<!--Задача 3: 
1)Сделайте 5 полей ввода, в которые вводятся числа.
2)Выведите на экран максимальное и минимальное из них. -->

<form action="" method="POST">
    <p><input type="number" name="one"></p>
    <p><input type="number" name="two"></p>
    <p><input type="number" name="three"></p>
    <p><input type="number" name="four"></p>
    <p><input type="number" name="five"></p>
    <p><input type="submit" name="GO"></p>
</form>

<?php
if (isset($_POST["one"]) && isset($_POST["two"]) && isset($_POST["three"]) && isset($_POST["four"]) && isset($_POST["five"])) {
    $one = $_POST["one"];
    $two = $_POST["two"];
    $three = $_POST["three"];
    $four = $_POST["four"];
    $five = $_POST["five"];
    if (isset($_POST["GO"])) {
        echo max($one, $two, $three, $four, $five) . "<br>";
        echo min($one, $two, $three, $four, $five) . "<br>";
    } else {
        echo "Чет не так";
    }
}

?>
<hr>
<!-- Задача 4:
1) Реализуйте заготовку регистрации пользователя.
2) Спросите у него логин или имейл, пароль (в браузере должен быть звездочками) и подтверждение пароля (тоже звездочками).
3) Логин должен содержать только буквы латинского алфавита и цифры, а также быть не больше 15 символов. Также, логин должен быть обязательным полем, если нет имейла.
4) Имейл должен быть валидным и обязательным, если нет логина
5) Сравните пароль и его подтверждение: если они не совпадают — выведите сообщение об этом. Также, ответ от сервера должен содержать код ошибки валидации - 422.
6) Сделайте так, чтобы скрипт обрезал концевые пробелы в строках, которые ввел пользователь.
7) Проверьте то, что пароль больше 5-ти символов и меньше 9-ти, а логин больше 3-х и меньше 12-ти символов. Если все правильно — выведите пользователю сообщение о том, что он успешно зарегистрирован.
8) Все данные формы должны отправлять только методом POST. -->

<form action="" method="POST">
    <p>Введите ваш логин</p>
    <p><input name="login" type="text" placeholder="Логин"></p>
    <p>Введите ваш e-mail</p>
    <p><input name="e_mail" type="text" placeholder="e-mail"></p>
    <p>Введите ваш пароль</p>
    <p><input name="password" type="password"></p>
    <p>Введите пароль еще раз</p>
    <p><input name="passwordTwo" type="password"></p></p>
    <p><input type="submit"></p>
</form>

<?php
/* Логин должен содержать только буквы латинского алфавита и цифры, а также быть не больше 15 символов*/
if (isset($_POST["login"])) {
    $loginEN = $_POST["login"];
    if ((ctype_alnum($loginEN) === true) && strlen($loginEN) <= 15) {
    } else {
        echo "Логин должен содержать только буквы латинского алфавита и цифры, а также быть не больше 15 символов" . "<br>";
    }
}
if (isset($_POST["e_mail"])) {
    $e_mail = $_POST["e_mail"];
    $email = filter_var($e_mail, FILTER_VALIDATE_EMAIL);
    if ($email == true) {
    } else {
        echo "Е-mail введен неверно" . "<br>";
    }
}
if (isset($_POST["password"]) && isset($_POST["passwordTwo"])) {
    $password = $_POST["password"];
    $passwordTwo = $_POST["passwordTwo"];
    if ($password === $passwordTwo) {
    } else {
        echo "Пароли не совпадают. Ошибка: 422 Unprocessable Entity" . "<br>";
    }
}
/* 7) Проверьте то, что пароль больше 5-ти символов и меньше 9-ти, а логин больше 3-х и меньше 12-ти символов. 
 Если все правильно — выведите пользователю сообщение о том, что он успешно зарегистрирован.*/
if (isset($_POST["password"])) {
    $password = $_POST["password"];
    $loginEN = $_POST["login"];
    $log = strlen($loginEN); // Количество символов в строке логин
    $pass = strlen($password); // Количество символов в строке пароль
    if (($log > 3 && $log < 12) && ($pass > 5 && $pass < 9)) {
        echo "Ура!!! Регистрация успешна";
    } else {
        echo "Пароль должен быть больше 5-ти символов и меньше 9-ти, а логин больше 3-х и меньше 12-ти символов";
    }
}
rtrim($_POST["e_mail"]);
rtrim($_POST["password"]);
rtrim($_POST["passwordTwo"]);
?>

<hr>
<!-- Задача 5:
1) Отправьте GET-запрос с помощью ссылки следующего вида: 'domain.сom/index.php?article=1&page=3'.
2) Запишите эти данные в переменные $page и $article.
 -->
<!-- Чесно, я не очень понял задание но наверно так -->
<form action="" method="GET">
    <p><input name="article" type="text" value="<?php if (isset($_REQUEST['article'])) echo $_REQUEST['article']; ?>">
    </p> <!-- Записываем данные в переменные -->
    <p><input name="page" type="text" value="<?php if (isset($_REQUEST['page'])) echo $_REQUEST['page']; ?>"></p>
    <!-- Записываем данные в переменные -->
    <p><input type="submit"></p>
</form>

<hr>
<!-- Задача 6:
1) Отправьте GET-запрос на показ определенной страницы и раздела сайта в формате: 'domain.сom/index.php?topic=cms&page=test.php'.
2) Запишите адрес страницы в переменную $page, а раздел в переменную $topic.
3) С помощью функции include() сделайте так,чтобы к странице index.php подключалась определенная страница ($page) из определенной папки ($topic).
4) Создайте 3 папки и 2 файла в каждой папке с различными названиями и с их помощью проверьте работу скрипта. -->
<!-- Вообще не понял как это воплотить -->
<hr>
<!-- Задача 7:
1) Спросите возраст пользователя с помощью формы.
2) Результат запишите в переменную $age.
3) Сделайте так, чтобы после отправки формы значения ее полей не пропадали. -->

<form action="" method="POST">
    <p>Введите ваш возраст</p>
    <p><input name="age" type="number" placeholder="Возраст"
              value="<?php if (isset($_REQUEST['age'])) echo $_REQUEST['age']; ?>"></p>
    <p><input type="submit"></p>
</form>
<?php
$age = $_POST["age"];
?>

<!-- Задача 8:
1) Спросите у пользователя, какие из языков он знает: java, python, php, javascript etc.
2) Выведите на экран те языки, которые знает пользователь.
 -->
<form action="" method="POST">
    <p>Какие из языков вы знаете:</p>
    <input type="checkbox" name="language[]" id="java" value="java"><label for="java"> Java</label>
    <input type="checkbox" name="language[]" id="python" value="python"><label for="python"> python</label>
    <input type="checkbox" name="language[]" id="php" value="php"><label for="php"> php</label>
    <input type="checkbox" name="language[]" id="javascript" value="javascript"><label for="javascript"> javascript
        etc</label><br><br>
    <input type="submit">
</form>
<?php
echo "<br>";
// Для проверки выбран ли елемент(елементи) из сиписка
$language = $_POST['language'];
if (empty($language)) {
    echo "Вы не выбрали ни одного язика.";
} else {
    // Для вывода выбранных элементов
    $lg = count($language);
    echo("Вы выбрали $lg язик(а): ");
    for ($i = 0; $i < $lg; $i++) {
        echo($language[$i] . " ");
    }
}
?>
<hr>
<!-- Задача 9:
1) Спросите у пользователя его возраст с помощью нескольких radio-кнопок.
2) Варианты ответа сделайте такими: менее 20 лет, 20-25, 26-30, более 30. -->

<form action="" method="POST">
    <p>Какой ваш возраст:</p>
    <p><input name="age20" type="radio">менее 20 лет</p>
    <p><input name="age25" type="radio">от 20-25 лет</p>
    <p><input name="age30" type="radio">от 26-30 лет</p>
    <p><input name="ageOld" type="radio">более 30 лет</p>
    <p><input type="submit"></p>
</form>

<?php
if (isset($_REQUEST['age20'])) {
    echo 'Вам менее 20 лет';
}
if (isset($_REQUEST['age25'])) {
    echo 'Вам 20-25 лет';
}
if (isset($_REQUEST['age30'])) {
    echo 'Вам 26-30 лет';
}
if (isset($_REQUEST['ageOld'])) {
    echo 'Вам более 30 лет';
}
?>

<hr>
<!-- Задача 10:
1)Спросите у пользователя его возраст с помощью select.
2)Варианты ответа сделайте такими: менее 20 лет, 20-25, 26-30, более 30. -->

<form action="" method="GET">
    <p>Сколько вам лет?</p>
    <select name="age">
        <p>
            <option value="age20">менее 20 лет</option>
        </p>
        <p>
            <option value="age25">20-25</option>
        </p>
        <p>
            <option value="age30">26-30</option>
        </p>
        <p>
            <option value="ageOld">более 30</option>
        </p>
    </select>
    <p><input type="submit"></p>
</form>

<?php
if (isset($_REQUEST['age']) and $_REQUEST['age'] == "age20") {
    echo 'Вам менее 20 лет';
}
if (isset($_REQUEST['age']) and $_REQUEST['age'] == "age25") {
    echo 'Вам 20-25 лет';
}
if (isset($_REQUEST['age']) and $_REQUEST['age'] == "age30") {
    echo 'Вам 26-30 лет';
}
if (isset($_REQUEST['age']) and $_REQUEST['age'] == "ageOld") {
    echo 'Вам более 30 лет';
}
?>
<hr>
<!-- Задача 11:
1) Сделайте функцию, которая создает обычный текстовый инпут.
2) Функция должна иметь следующие параметры: type, name, value.
3) Функция должна сохранять значение инпута после отправки.-->
<?php
function input($type, $name, $value)
{
    /*    if(isset($_REQUEST[$name])) {
            $value = $_REQUEST[$name];
        }*/
    return "<input type= $type name= $name value= $value>";
}

echo input('number', 'name', '1488');
?>

<!-- Задача 12:
1) Дан массив [1, 2, 3, 4,5];
2) Выведите каждый элемент этого массива в отдельном абзаце. -->
<?php
$array = [1, 2, 3, 4, 5];
// Думаю неправильно но по другому незнаю как
function p($arr)
{
    foreach ($arr as $value) {
        echo '<p>' . $value . '</p>';
    }
}

echo p($array);
?>
<hr>
<?php
/* Задача 13:
1) Дан массив:
	$arr = [
		['name'=>'Коля', 'age'=>30, 'salary'=>500],
		['name'=>'Вася', 'age'=>31, 'salary'=>600],
		['name'=>'Петя', 'age'=>32, 'salary'=>700],
    ];
2) С помощью цикла сформируйте с его помощью следующий HTML код:
	<tr>
		<tr>
			<td>Коля</td>
			<td>30</td>
			<td>500</td>
		</tr>
		<tr>
			<td>Вася</td>
			<td>31</td>
			<td>600</td>
		</tr>
		<tr>
			<td>Петя</td>
			<td>32</td>
			<td>700</td>
		</tr>
	</tr>*/
$arr = [
    ['name' => 'Коля', 'age' => 30, 'salary' => 500],
    ['name' => 'Вася', 'age' => 31, 'salary' => 600],
    ['name' => 'Петя', 'age' => 32, 'salary' => 700],
];
echo "<table>";
foreach ($arr as $key => $name) {
    foreach ($name as $value) {
        echo "<tr><tr><td>$value</td></tr></tr>";
    }
}
echo "</table>"
?>
<hr>
<!-- Задача 14:
1) Отправьте с помощью GET-запроса два числа. Выведите его на экран сумму этих чисел.	
2) Пусть с помощью GET-запроса отправляется число. Оно может быть или 1, или 2. Сделайте так, чтобы если передано 1 - на экран вывелось слово 'привет', а если 2 - то слово 'пока'. -->
<?php
// Отправьте с помощью GET-запроса два числа. Выведите его на экран сумму этих чисел.
echo $_GET['get0'] + $_GET['get2'] . "<br>";
// Пусть с помощью GET-запроса отправляется число. Оно может быть или 1, или 2. Сделайте так, чтобы если передано 1 - на экран вывелось слово 'привет', а если 2 - то слово 'пока'.
if ($_GET['get0'] == 1) {
    echo "привет" . "<br>";
} elseif ($_GET['get0'] == 2) {
    echo "пока" . "<br>";
} else {
    echo "Хьюстон, у нас проблемы" . "<br>";
}
?>
<hr>
<!-- Задача 15:
1) Дан массив.
2) Сделайте так, чтобы с помощью GET-запроса можно было вывести любой элемент этого массива.
3) Для этого вручную сделайте ссылку для каждого элемента массива. 
4) Переходя любой ссылке мы должны увидеть на экране соответствующий элемент массива. 	
5) Ссылки должны выводились с помощью цикла foreach автоматически в соответствии с количеством элементов в массиве. -->
<?php
$array = ["zero", "one", "two", "three", "four", "five"];
echo $_GET["get"] . "<br><br>";
/*3) Для этого вручную сделайте ссылку для каждого элемента массива. 
Немного не понятно зачем создавать ссылки когда можно сразу через foreach
<a href="?get=zero">zero</a><br>
<a href="?get=one">one</a><br>
<a href="?get=two">two</a><br>
<a href="?get=three">three</a><br>
<a href="?get=four">four</a><br>
<a href="?get=five">five</a><br>*/
foreach ($array as $key => $value) {
    echo "<a href=?get=$value> $value </a> <br>";
}
?>
<hr>

<!-- Задача 16:
1) Сделайте страницу, при заходе на которую пользователя будет сразу же редиректить на другую страницу в вашем проекта.
2) Со второй страницы должно редиректить на сайт Itea по истечению 3 секунд с момента захода на нее. -->
<!-- В папке task16 -->


</body>
</html>